FROM ubuntu
ARG VERSION

RUN echo "Building version $VERSION" && apt update -y && apt install -y wget tar && \
  wget https://github.com/getzola/zola/releases/download/${VERSION}/zola-${VERSION}-x86_64-unknown-linux-gnu.tar.gz -O zola.tar.gz && \
  mkdir -p /opt/zola-${VERSION} && \
  tar -xzf zola.tar.gz -C /opt/zola-${VERSION}/ && \
  rm zola.tar.gz

ENV PATH $PATH:/opt/zola-${VERSION}

CMD ["sh", "-c", "zola build"]
